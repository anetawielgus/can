#include "can.h"

//----C1MOD CAN1 Mode Register---------------------------------
#define RESET_bm 0x00000001
#define RELEASE_bm 0
//-------C1BTR CAN1 Bus Timing Register------------------------
#define TIMING_bm 0x001C001D																							// Set bit timing to 125k
//-------PINSEL1-----------------------------------------------
#define RD1_bm 1<<18
#define TD2_bm 1<<16
#define RD2_bm 1<<14
//-------C1TFI1 CAN1 Tx Frame Information Register (buffer 1)--
#define DATA_LENGTH_bm 0x00040000
//-------AFMR Acceptance Filter Mode Register------------------
#define AccOff_bm 1<<0
#define AccBP_bm 1<<1
//------C2CMR - CAN2 Command Register--------------------------
#define RRB_bm 1<<2																											// Release Reciever Buffer
//------C1SR - CAN1 Status Register----------------------------
#define TBS1_bm 1<<2
//------C2SR - CAN2 Status Register----------------------------
#define RBS_bm 1<<0
//------C1TID1 - CAN1 Tx Identifier Register (buffer 1)-------
#define STANDARD_IDENTIFIER 0x00000022
//------C1CMR - CAN1 Command Register-------------------------
#define TR_bm 1<<0

void Can1_InitAsTX(){
	PINSEL1 = PINSEL1 | RD1_bm;
	
	C1MOD = RESET_bm;
	C1BTR = TIMING_bm;
	C1MOD = RELEASE_bm;
	
	C1TFI1 = DATA_LENGTH_bm;
}
void Can2_InitAsRX(unsigned int uiID){
	PINSEL1 = PINSEL1 | TD2_bm | RD2_bm;
	
	C2MOD = RESET_bm;
	C2BTR = TIMING_bm;
	C2MOD = RELEASE_bm;
	
	AFMR = AFMR|AccBP_bm|AccOff_bm;
	AFRAM = 0x20004000|uiID;
	SFF_sa = 0x00000000;														//Set start address of Standard table
  SFF_GRP_sa = 0x00000008;												//Set start address of Standard group table
  EFF_sa = 0x00000008;														//Set start address of Extended table
  EFF_GRP_sa = 0x00000008;												//Set start address of Extended group table
  ENDofTable = 0x00000008;												//Set end of table address
	AFMR = AFMR&(~(AccBP_bm))&(~(AccOff_bm));
	
	C2CMR = RRB_bm;
}

enum eTxStatus ucCan1_TxFree(){
	if(C1SR & TBS1_bm) {
		return FREE;
	}
	else{
		return BUSY;
	}
}
enum eRxStatus ucCan2_RxReady(){
	if(C2SR & RBS_bm){
		return READY;
	}
	else{
		return NOTREADY;
	}
}

void Can1_Send(unsigned char ucSendMessage){
	C1TID1 = STANDARD_IDENTIFIER;
	C1TDA1 = ucSendMessage;
	C1CMR = TR_bm;
}

void Can1_SendByte(unsigned int uiID,unsigned char ucSendMessage){
	C1TID1 = uiID;
	C1TDA1 = ucSendMessage;
	C1CMR = TR_bm;
}

unsigned char ucCan2_Receive(void){
	unsigned char ucReceivedData = C2RDA;
	C2CMR = RRB_bm;
	return ucReceivedData;
}

unsigned char ucCan2_ReceiveByte(void){
	unsigned char ucReceivedData = C2RDA;
	C2CMR = RRB_bm;
	return ucReceivedData;
}
