#include <LPC21xx.H>
#include "spi.h"

#define SPI_CLOCK_DIVIDER 0x08

#define SCK_bm  1<<8
#define MISO_bm 1<<10
#define MOSI_bm 1<<12
#define SSEL_bm 1<<14

#define CS_bm   1<<10

#define CPHA_bm 1<<3
#define CPOL_bm 1<<4
#define MSTR_bm 1<<5
#define LSBF_bm 1<<6

#define DAC_SHDN 1<<4
#define DAC_GAIN 1<<5
#define DAC_BUFF 1<<6
#define DAC_A_CHANNEL 1<<7

#define SPIF_bm 1<<7

void DAC_MCP4921_Set(unsigned int uiVoltage){
	PINSEL0 = PINSEL0 | SCK_bm | MISO_bm | MOSI_bm | SSEL_bm;;
	IO0DIR = IO0DIR | CS_bm;
	S0SPCR = S0SPCR | MSTR_bm;
	S0SPCR = S0SPCR & (~(CPHA_bm)) & (~(LSBF_bm)) & (~(CPOL_bm));
	S0SPCCR = SPI_CLOCK_DIVIDER;
	IO0SET = CS_bm;
	
	IO0CLR = CS_bm;
	S0SPDR = DAC_GAIN | DAC_SHDN | ((0xFF00 & uiVoltage)>>8) ;
	while((S0SPSR & SPIF_bm) == 0){}
	S0SPDR = 0x00FF & uiVoltage;
	while((S0SPSR & SPIF_bm) == 0){}
	IO0SET = CS_bm;
}
