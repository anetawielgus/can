#include <LPC21xx.H>

enum eTxStatus {BUSY, FREE};
enum eRxStatus {NOTREADY, READY};

void Can1_InitAsTX(void);
void Can2_InitAsRX(unsigned int);

enum eTxStatus ucCan1_TxFree(void);
enum eRxStatus ucCan2_RxReady(void);

void Can1_Send(unsigned char);
unsigned char ucCan2_Receive(void);

void Can1_SendByte(unsigned int ,unsigned char);
unsigned char ucCan2_ReceiveByte(void);
