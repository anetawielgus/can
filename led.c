#include <LPC21xx.H>
#include "led.h"

#define LED0_bm 1<<16
#define LED1_bm 1<<17
#define LED2_bm 1<<18
#define LED3_bm 1<<19
#define LED4_bm 1<<20
#define LED5_bm 1<<21
#define LED6_bm 1<<22
#define LED7_bm 1<<23
#define ALL_LEDS_bm (LED0_bm | LED1_bm | LED2_bm | LED3_bm | LED4_bm | LED5_bm | LED6_bm | LED7_bm)
#define FIRST_LED_BIT_bm 16	
	
	void Led_Init() {
		IO1DIR = IO1DIR | ALL_LEDS_bm;
		IO1CLR = ALL_LEDS_bm;
		//IO1SET = LED0_bm;
	}
	
	void Led_On(unsigned char ucLedIndeks) {
		IO1CLR = ALL_LEDS_bm;
		switch (ucLedIndeks) {
			case 0:
				IO1SET = LED0_bm;
				break;
			case 1:
				IO1SET = LED1_bm;
				break;
			case 2 :
				IO1SET = LED2_bm;
				break;
			case 3 :
				IO1SET = LED3_bm;
				break;
			case 4 :
				IO1SET = LED4_bm;
				break;
			case 5 :
				IO1SET = LED5_bm;
				break;
			case 6 :
				IO1SET = LED6_bm;
				break;
			case 7 :
				IO1SET = LED7_bm;
				break;
			default:
				break;
		
			
	}
}
enum StepDirection {LEFT,RIGHT};
		
		void Led_Step(enum StepDirection eDirection){
			static unsigned int uiCycleCounter;
			switch (eDirection){
				case LEFT:
					uiCycleCounter = (++uiCycleCounter) % 4;
				break;
			  case RIGHT:
					uiCycleCounter = (--uiCycleCounter) % 4;
			  break;
		}
		Led_On(uiCycleCounter);
	}
	
	void Led_StepLeft(){
		Led_Step(LEFT);
	}
	
	void Led_StepRight(){
		Led_Step(RIGHT);
	}
	
void Led_ShowByte(unsigned char ucLedByte){
	IOCLR1 = ALL_LEDS_bm;
  IOSET1 = (ucLedByte << FIRST_LED_BIT_bm) & ALL_LEDS_bm;
}	
