#include <LPC21xx.H>
#include "i2c.h"

//-----Pin Function Select Register 0 (PINSEL0)-------
#define SCL_bm 1<<4
#define SDA_bm 1<<6

//-----Control Set Register(I2CONSET)-----------------
#define I2EN_bm 1<<6
#define STA_bm 1<<5
#define STO_bm 1<<4

//-----Control Clear Register (I2CONCLR)--------------
#define AAC_bm 1<<2
#define SIC_bm 1<<3
#define STAC_bm 1<<5
#define I2ENC_bm 1<<6

//-----SCL Duty Cycle---------------------------------
#define DUTY_CYCLE_HIGH 0x80
#define DUTY_CYCLE_LOW 0x80

//-----Interrupt Enable Register (VICIntEnable)-------
#define VIC_I2C_CHANNEL_NR 9

//-----VICVectCntlx Vector Control Registers----------
#define mIRQ_SLOT_ENABLE 0x00000020

//-----Slave Address----------------------------------
#define SLAVE_ADDR_W 0x40
#define SLAVE_ADDR_R 0x41

//-----Status Codes-----------------------------------
#define START_BIT_STATUS 0x08
#define SLAVE_ADDR_ACK 0x18
#define SLAVE_ADDR_NOT_ACK 0x20
#define DATA_SENT_ACK 0x28

unsigned int ucI2Cdata = 0;
unsigned int uiI2CSlaveAddres = 0;

//(Interrupt Service Routine) of I2C interrupt
__irq void I2CIRQHandler (void) 															// I2C interrupt routine
{
unsigned int uiI2CStatus = I2STAT;														// Read result code
	
	switch (uiI2CStatus)	{ 																		// Switch to next action
	case (START_BIT_STATUS): 																		// Start bit
		I2CONCLR = STAC_bm; 																			// Clear start bit
		I2DAT = uiI2CSlaveAddres; 																// Send address and write bit
		break;
	case (SLAVE_ADDR_ACK): 																			// Slave address+W, ACK
		I2DAT = ucI2Cdata; 																				// Write data to tx register
		break;
	case (SLAVE_ADDR_NOT_ACK): 																	// Slave address +W, Not ACK
		I2DAT = uiI2CSlaveAddres; 																// Resend address and write bit
		break;
	case (DATA_SENT_ACK): 																			// Data sent, Ack
		I2CONSET = STO_bm; 																				// Stop condition
		break;
	default :
		break;
	}
	I2CONCLR = I2CONCLR | SIC_bm; 															// Clear I2C interrupt flag
	VICVectAddr = 0x00000000; 																	// Clear interrupt 
}


void I2C_Init(void){
	PINSEL0 = PINSEL0 | SCL_bm | SDA_bm;												// Set pin functions 
	I2CONCLR = I2CONCLR | AAC_bm | SIC_bm |STAC_bm |I2ENC_bm;		// Clear bits
	I2SCLH = DUTY_CYCLE_HIGH;																		
	I2SCLL = DUTY_CYCLE_LOW;																
	I2CONSET = I2EN_bm;																					// Set transmition mode
	
	VICIntEnable |= (0x1 << VIC_I2C_CHANNEL_NR);           		 // Enable I2C interrupt
	VICVectCntl0  = mIRQ_SLOT_ENABLE | VIC_I2C_CHANNEL_NR; 		 // Enable Slot 0 and assign it to I2C interrupt channel
	VICVectAddr0  = (unsigned long) I2CIRQHandler; 	       		 // Set to Slot 0 Address of Interrupt Service Routine 
}

void PCF8574_Write(unsigned char ucData){
	ucI2Cdata = ucData;
	uiI2CSlaveAddres = SLAVE_ADDR_W;

	I2CONCLR = I2CONCLR | AAC_bm | SIC_bm |STAC_bm |I2ENC_bm;	// Clear interrupt flag
	I2CONSET = I2EN_bm;																				// Set transmition mode
	I2CONSET = STA_bm;
}
